FROM ubuntu:18.04

RUN apt-get -y update
RUN apt-get update --fix-missing
RUN apt-get install -y wget build-essential astyle python3.7 git

# username: user, password: user, Home: /home/user, Groups: sudo, user
RUN /usr/sbin/useradd -m -s /bin/bash -G sudo user && echo user:user | /usr/sbin/chpasswd

WORKDIR /home/user/

# Per original bootloader readme
RUN wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/7-2017q4/gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2
# TODO: test with newer one?
#https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
RUN tar -xjf gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2 -C /usr/share/

RUN ln -s /usr/share/gcc-arm-none-eabi-7-2017-q4-major/bin/arm-none-eabi-gcc /usr/bin/arm-none-eabi-gcc
RUN ln -s /usr/share/gcc-arm-none-eabi-7-2017-q4-major/bin/arm-none-eabi-ar /usr/bin/arm-none-eabi-ar
RUN ln -s /usr/share/gcc-arm-none-eabi-7-2017-q4-major/bin/arm-none-eabi-g++ /usr/bin/arm-none-eabi-g++
RUN ln -s /usr/share/gcc-arm-none-eabi-7-2017-q4-major/bin/arm-none-eabi-gdb /usr/bin/arm-none-eabi-gdb
RUN ln -s /usr/share/gcc-arm-none-eabi-7-2017-q4-major/bin/arm-none-eabi-size /usr/bin/arm-none-eabi-size
RUN ln -s /usr/share/gcc-arm-none-eabi-7-2017-q4-major/bin/arm-none-eabi-objcopy /usr/bin/arm-none-eabi-objcopy

RUN ln -s /usr/bin/python3.7 /usr/bin/python

USER user