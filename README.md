# px4-bootloader-build-docker

Docker Project for building the PX4 bootloader

## Summary

This project is used to generate a docker image that can then be used as a build host.  This means you don't have to deal with setting up tools, which can be a great deal of pain!

It does mean you need to have a bare minimum knowledge of git, bash and Docker.

## Requirements

- Git
- Docker

## To Build Image

Use helper script:

```
./docker-build-image.sh
```

## To Use Image

Checkout the bootloader project:

```
git clone -b voxl2-io https://github.com/modalai/px4-bootloader.git
cd px4-bootloader
git submodule sync --recursive
git submodule update --init --recursive
```

Now, copy the `docker-run-image.sh` script to the root of the bootloader project and run it (or inspect the contents of the script and run yourself).

```
cd px4-bootloader
cp ../px4-bootloader-build-docker/docker-run-image.sh .
./docker-run-image.sh
make clean
make modalai_voxl2_io_bl
```

Binary is here:

```
user@8ac1f9b086cf:~$ ls build/modalai_voxl2_io_bl/*.bin
build/modalai_voxl2_io_bl/modalai_voxl2_io_bl.bin
```
