#!/bin/bash
BUILD_OUTPUT=workspace
IMAGE_NAME=px4-bootloader-build
IMAGE_TAG=0.0

echo "[INFO] Building Image: ${IMAGE_NAME}:${IMAGE_TAG}"
docker build -t "${IMAGE_NAME}:${IMAGE_TAG}" .